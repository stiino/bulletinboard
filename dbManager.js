var mongoClient = require('mongodb').MongoClient
var querystring = require("querystring");
var dbInfo;

function connectDB(){
    mongoClient.connect("mongodb://127.0.0.1/bulletinBoard",function(err,db){
	if(err){
	   return console.dir(err);
	}

	console.log("connected db");

	db.collection("test",function(err,collection){
	    if(err) {return console.dir(err);}
	    dbInfo = collection;
	});
    });
}

function find(response,commingData){
    console.log("comming find");
    dbInfo.find().toArray(function(err,result){
	if(err){return console.log(err);}
	console.log(result[0].text + " " + result[0].id);
	var resText = "resText:";
	for(var length = 0 ; length < result.length ; length++){
	    console.log(length);
	    if(result[length].text){
		resText += result[length].text + "," + result[length].id + ",";
		console.log(resText);
		}
	} 
	response.writeHead(200,{'Content-Type':'text/plain'});
	response.write(resText);
	response.end();
    });
}

function insert(response,commingData){
    console.log("comming insert id:" + querystring.parse(commingData).id + "text:" + querystring.parse(commingData).text);
    dbInfo.insert({"id":querystring.parse(commingData).id ,"text":querystring.parse(commingData).text})
    response.writeHead(200,{'Content-Type':'text/plain'});
    response.write("insert success");
    response.end();
}

function update(response,commingData){
    console.log("comming update id:" + querystring.parse(commingData).id + "text:" + querystring.parse(commingData).text);
    dbInfo.update({"id":querystring.parse(commingData).id},{$set:{"text":querystring.parse(commingData).text}},function(err){
	if(err){return console.log(err)}
    });
    response.writeHead(200,{'Content-Type':'text/plain'});
    response.write("update success");
    response.end();
}

function remove(response,commingData){
    console.log("comming remove id:" + querystring.parse(commingData).id);
    dbInfo.remove({"id":querystring.parse(commingData).id},function(err,numberOfRemovedDocs){
    if(err){return console.log(err)}
    console.log(numberOfRemovedDocs)}
    );
    response.writeHead(200,{'Content-Type':'text/plain'});
    response.write("update success");
    response.end();
}

exports.connectDB = connectDB;
exports.find = find;
exports.insert = insert;
exports.update = update;
exports.remove = remove;
