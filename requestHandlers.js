var dbManager = require("./dbManager");

function find(responce,commingData){
    //データを取得する
    dbManager.find(responce,commingData);
}

function insert(response,commingData){
    //データを登録する処理
    dbManager.insert(response,commingData);
}

function update(response,commingData){
    dbManager.update(response,commingData);
}

function remove(response,commingData){
   dbManager.remove(response,commingData); 
}

exports.find = find;
exports.insert = insert;
exports.update = update;
exports.remove = remove;
