var server = require("./server");
var router = require("./router");
var requestHandlers = require("./requestHandlers");
var dbManager = require("./dbManager");

var handle = {}
handle["/find"] = requestHandlers.find;
handle["/insert"] = requestHandlers.insert;
handle["/update"] = requestHandlers.update;
handle["/remove"] = requestHandlers.remove;

dbManager.connectDB();
server.start(router.route,handle);
