var http = require("http");
var url = require("url");
var dbManager = require("./dbManager");

function start(route,handle){
    function onRequest(request,response){
	console.log("comming onRequest");
	var postData = "";
	var urlInfo = url.parse(request.url);
	if(request.method === 'POST'){
	    console.log("requestMethod is POST");
	    methodType = "POST";
	    request.setEncoding("utf8");

	    request.addListener("data",function(postDataChunk){
		postData += postDataChunk;
	    });

	    request.addListener("end",function(){
		console.log("AddListener end");
		console.log(urlInfo);
		route(handle,urlInfo.pathname,response,postData);
	    });
	}else if(request.method === 'GET'){
	    console.log("requestMethod is GET");
	    methodType = "GET";
	    route(handle,urlInfo.pathname,response,urlInfo.query);
	}	
    }

	http.createServer(onRequest).listen(8888);
	console.log("Server has started.");
}

exports.start = start;
